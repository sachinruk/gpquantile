clear all
% close all
clc

alpha=0.1;
sigma=0.1;
f=3;
y=-10:0.1:10;
e=y-f;
lnp=log(alpha)+log(1-alpha)-log(sigma)-e.*(alpha-(e<0))/sigma;
p=exp(lnp);

figure; subplot(121); plot(y,p); subplot(122); plot(y,lnp)