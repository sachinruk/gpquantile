function loss=percentageLoss(y,f,alpha)
loss=abs(sum(y<f)/length(y)-alpha);