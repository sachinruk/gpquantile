clear all
close all
clc

load caution.mat

x=caution(:,1:2); y=caution(:,3); y=(y-mean(y))/std(y);
% plot(x,y,'.'); hold on;

alpha=0.5;
%%%%%%%%%%%%%%%%%%%
%My method
cov=@covSEard;
hyp0.cov = rand(3,1);
[muVB,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha,hyp0.cov);
[y_star, var_star]=vbQPredict(cov,x,x,params,alpha,muVB,Sigma);
std_star=sqrt(var_star);
% plot(x,muVB,'r.')

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%EM method
likfunc = @likALD; 
hyp0.lik = [log(1); alpha];

hyp = minimize(hyp0, @gp, -4000, @infEP, [], cov, likfunc, x, y); % maximum likelihood
% hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
% Prediction
[ymu, ys2, muEP, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
% plot(x,muEP,'m.')


%%%%%%%%%%%%%%%%%%%%%%
%%linear method
new_x=[ones(length(x),1) x x.^2 x.^3 x.^4 x.^5 x.^6 x.^7];
[beta]=rq_fnm(new_x,y,alpha);
muLin=new_x*beta; %prediction
% plot(x,muLin,'k.')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

% legend('observations', 'VB inference', 'EP inference', 'linear')

pinballLoss(y,muVB,alpha)
pinballLoss(y,muEP,alpha)
pinballLoss(y,muLin,alpha)