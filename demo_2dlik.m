clear all
close all
clc
addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;

y=[1;2];

alpha=0.1;
f1=-3:0.1:3;
f2=-3:0.1:3;

e1=y(1)-f1; e2=y(2)-f2; I1=e1<0; I2=e2<0;
pf1=exp(-e1.*(alpha-I1))'; pf2=exp(-e2.*(alpha-I2))';
pf12=pf1*pf2';
figure; surf(f1,f2,pf12); xlabel('f_1'); ylabel('f_2'); zlabel('p(f_1,f_2)');

figure; plot(f1,pf1);
hyp.cov=[0 1]; hyp.lik=-100; 
x_train=[-1; 1; 2]; e=y(1)-x_train; I=e<0; y_train=exp(-e.*(alpha-I)); 
hold on; plot(x_train,y_train,'rx')
hyp = minimize(hyp, @gp, -1000, @infExact, [], @covSEard, @likGauss, x_train, y_train);
hyp2=minPosterior(x_train,y_train,f1',pf1,[0; 0; -1]);
gp_p=gp(hyp2, @infExact, [], @covSEard, @likGauss, x_train, y_train, f1');
hold on; plot(f1,gp_p,'g');