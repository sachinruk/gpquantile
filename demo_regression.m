clear all
close all
clc

addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;

sigma= 3; alpha=0.95;
beta=[2 5];
rng(1);

N=500;
%simulate the errors
u=rand(N,1); w=-log(1-u); z=randn(N,1);
e=((1-2*alpha)/(alpha*(1-alpha)))*sigma*w+...
                        sqrt(2*w/(alpha*(1-alpha)))*sigma.*z;
x=[ones(N,1) 10*randn(N,1)+5]; y=x*beta'+e; %simulate observations
x=x(:,2);
%VB step
idx=randperm(length(x)); n_obs=200;
x_train=x(idx(1:n_obs),:); y_train=y(idx(1:n_obs));
x_test=x(idx(n_obs+1:end),:); y_test=y(idx(n_obs+1:end));

cov=@covSEard; params=rand(3,1);
[mu,Sigma,~,~,a,b,params]=vbGP(y_train,x_train,cov, alpha, params);
[y_star, var_star]=vbPredict(cov,x_test,x_train,params,alpha,a,b,mu,Sigma);
std_star=sqrt(var_star);

mean(abs(y_star-y_test))

[x_test2, idx]=sort(x_test); 
std_envelope=[y_star(idx)-std_star(idx); flipdim(y_star(idx)+std_star(idx),1)];
figure; fill([x_test2; flipdim(x_test2,1)],std_envelope , [7 7 7]/8);
hold on;
plot(x_test2,y_star(idx),'r')
plot(x_test2, y_test(idx),'b.')
plot(x_train,y_train,'g.')

c1=(alpha*(1-alpha)); c2=(1-2*alpha)/c1;
q=[ones(n_obs,1) x_train]*beta'+c2*b/(a-1);
plot(x_train,q,'c.')
plot(x,[ones(N,1) x]*beta','k.')

