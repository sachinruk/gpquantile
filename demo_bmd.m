clear all
close all
clc

addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;
addpath(genpath('minFunc_2012'))
warning('off')

load bmd.mat
x=age; y=density; y=(y-mean(y))/std(y);
[x, idx]=sort(x); y=y(idx);

iterations=20;
alpha=[0.01,0.1 0.5 0.9 0.99]; 
cov=@covSEard;

VBLoss=zeros(iterations,length(alpha));
EPLoss=zeros(iterations,length(alpha));
linLoss=zeros(iterations,length(alpha));

VBPLoss=zeros(iterations,length(alpha));
EPPLoss=zeros(iterations,length(alpha));
linPLoss=zeros(iterations,length(alpha));

% VBHyp=zeros(3,iterations);
% EPHyp=zeros(3,iterations);
rng(1);
for i=1:iterations
    ell=rand; sf=rand; sn=rand;%[ell, sf, sn]=initgppar(x,y); 
    idx=randperm(length(x)); train_idx=idx(1:388); test_idx=idx(389:end);
    xtr=x(train_idx,:); ytr=y(train_idx);
    x_test=x(test_idx,:); y_test=y(test_idx);
%     figure; plot(xtr,ytr,'.'); xlabel('Age'); ylabel('Density'); hold on;  
    for j=1:length(alpha)    
        %%%%%%%%%%%%%%%%%%%
        %My method
        [mu,Sigma,~,~,~,~,params]=vbGP(ytr,xtr,cov, alpha(j),[ell; sf; sn]);
        [VB_star]=vbQPredict(cov,x_test,xtr,params,mu,Sigma);
%         plot(x,VB_star,'m')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %EM method
        likfunc = @likALD;
        hyp0.lik = [sn; alpha(j)]; hyp0.cov = [ell; sf];
        hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, ...
                                                           xtr, ytr,0);
        % Prediction
        try
            [~, ~, EP_star] = gp(hyp, @infEP, [], cov, likfunc, xtr, ytr, x_test);
%             plot(x,EP_star,'k');
        catch
            EP_star=nan(size(VB_star));
        end
%         EPHyp(:,i)=[hyp.cov; hyp.lik(1)];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %linear method
        new_x=[ones(length(xtr),1) xtr xtr.^2 xtr.^3 xtr.^4 xtr.^5 xtr.^6 xtr.^7];
        [beta]=rq_fnm(new_x,ytr,alpha(j));
        x_query=ones(length(x_test),1);
        for k=1:7, x_query=[x_query x_test.^k]; end
        q_linear_pred=x_query*beta; %prediction
%         plot(x,q_linear_pred,'r')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

        VBLoss(i,j)=pinballLoss(y_test,VB_star,alpha(j));
        EPLoss(i,j)=pinballLoss(y_test,EP_star,alpha(j));   
        linLoss(i,j)=pinballLoss(y_test,q_linear_pred,alpha(j));
        
        VBPLoss(i,j)=percentageLoss(y_test,VB_star,alpha(j));
        EPPLoss(i,j)=percentageLoss(y_test,EP_star,alpha(j));   
        linPLoss(i,j)=percentageLoss(y_test,q_linear_pred,alpha(j));
    end
%     legend('Observations','VB Quantiles','EP Quantiles');
    disp(i);
end

for j=1:length(alpha)
    disp([mean(VBLoss(:,j)) std(VBLoss(:,j)) nanmean(EPLoss(:,j)) nanstd(EPLoss(:,j)) mean(linLoss(:,j)) std(linLoss(:,j))]);
    disp([mean(VBPLoss(:,j)) std(VBPLoss(:,j)) nanmean(EPPLoss(:,j)) nanstd(EPPLoss(:,j)) mean(linPLoss(:,j)) std(linPLoss(:,j))]);
end

% alpha=0.05:0.05:0.95;
% figure; plot(x,y,'.'); hold on;
% ell=rand; sf=rand; sn=rand; 
% for i=1:length(alpha)
%     [mu,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha(i),[ell; sf; sn]);
%     [VB_star]=vbQPredict(cov,x,x,params,mu,Sigma);    
%     plot(x,VB_star,'r','LineWidth',2);
% end
% xlabel('x'); ylabel('y'); legend('Observations','Inferred quantile');
% 
% figure; plot(x,y,'.'); hold on; %plot(x,m,'r')
% hyp0.cov = rand(2,1);
% for i=1:length(alpha)
%     hyp0.lik = [rand; alpha(i)];
%     hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
%     [ymu, ys2, fmu, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
%     plot(x,fmu,'r','LineWidth',2)
% end
% xlabel('x'); ylabel('y'); legend('Observations','Inferred quantile');
