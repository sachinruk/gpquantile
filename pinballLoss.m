function l=pinballLoss(y,f,alpha)

eps=y-f;
eps(eps>0)=alpha*eps(eps>0);
eps(eps<0)=(alpha-1)*eps(eps<0);
l=mean(eps);