function [q, var_q]=vbPredict(cov,x,X,params,alpha,a,b,mu,Sigma)

K11=cov(params,x,x); 
K12=cov(params,x,X); 
K=cov(params,X,X); 
d=eig(K); d=real(d);
if min(d)<0 %if slightly negative definite
    step=length(K); 
    K(1:(step+1):end)=K(1:(step+1):end)-min(d)+1e-7;    
end
[U, D]=svd(K); d=diag(D); %adjusted kernel matrix
Kinv=diagMult(U,1./d,'r')*U';
K12Kinv=K12*Kinv;

c1=(alpha*(1-alpha)); c2=(1-2*alpha)/c1;
q=K12Kinv*mu+c2*b/(a-1);

gp_var=K11-K12Kinv*K12';
c3=1/c1+c2^2;
var_q=c3*2*b^2/((a-1)*(a-2))-c2^2*b^2/(a-1)^2+diag(gp_var+K12Kinv*Sigma*K12Kinv');