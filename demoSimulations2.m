clear all
% close all
clc

N=10000;
alpha=0.01:0.01:0.98;
a2=zeros(98,1);
for i=1:98
    w=randraw('gamma', [0 5 1], N); %w=1./w;
    y=randn(N,1).*sqrt(2*w./(alpha(i)*(1-alpha(i))))+(1-2*alpha(i))*w./(alpha(i)*(1-alpha(i)));
    a2(i)=sum(y<0)/length(y);
end

figure; plot(alpha,a2);