function [q, var_q]=vbQPredict(cov,x,X,params,mu,Sigma)
sigma2=exp(2*params(end));
K11=cov(params(1:(end-1)),x,x); 
K12=cov(params(1:(end-1)),x,X); 
K=cov(params(1:(end-1)),X,X); 
N=length(K); K(1:(N+1):end)=K(1:(N+1):end)+sigma2;
L=jitChol(K);
% d=eig(K); d=real(d);
% if min(d)<0 %if slightly negative definite     
%     K(1:(N+1):end)=K(1:(N+1):end)-min(d)+1e-7;    
% end
% [U, D]=svd(K); d=diag(D); %adjusted kernel matrix
% Kinv=diagMult(U,1./d,'r')*U';
% K12Kinv=K12*Kinv;
K12Linv=K12/(L');
Linvmu=L\mu;

% c1=(alpha*(1-alpha)); c2=(1-2*alpha)/c1;
q=K12Linv*Linvmu;

gp_var=diag(K11)-sum(K12Linv.^2,2);
L2=jitChol(Sigma);
LinvSigma_sqrt=L\L2;
K12KinvL2=K12Linv*LinvSigma_sqrt;
% c3=1/c1+c2^2;
var_q=gp_var+sum(K12KinvL2.^2,2);