clear all
close all
clc

f=-10:0.1:10;
y=7;
alpha=0.5;
lnC=log(alpha)+log(1-alpha);
e=y-f;
lnAsymLaplace=lnC-e.*(alpha-double(e<0));
figure; plot(f,lnAsymLaplace);
lnGauss=-0.5*(log(2*pi)+f.^2);
hold on; plot(f,lnGauss,'r');
plot(f,lnGauss+lnAsymLaplace,'g');

figure; plot(f,exp(lnAsymLaplace));
hold on; plot(f,exp(lnGauss),'r');
postC=sum(exp(lnGauss+lnAsymLaplace))*0.1;
plot(f,exp(lnGauss+lnAsymLaplace)/postC,'g');


figure;plot(f,exp(lnGauss+lnAsymLaplace),'g');
w=2;
for k=1:6
    LB=log(0.5^2)-0.5*e.^2/w-w/8;
    pfyLB=lnGauss+LB;
    hold on; plot(f,exp(pfyLB),'r-.');
%     postSigmaInv=(alpha*(1-alpha)/(2*w)+1)^(-1); L=chol(postSigmaInv,'lower');
%     mu= 0.5*(postSigmaInv\(alpha*(1-alpha)*y/w-(1-2*alpha)));
%     eSqr=((f-mu)*L).^2; 
%     lnPostApproxF=-0.5*log(2*pi)+log(diag(L))-0.5*eSqr;
%     hold on; plot(f,exp(lnPostApproxF),'r-.');
    w=w*1.5;
end
legend('True Joint Distribution', 'approximation');
xlabel('f'); ylabel('p(y,f)')
