function [mu,Sigma,alpha_,beta,a,b,params]=vbGP(y,X,cov,alpha,params)
%performs variataional bayes and computes the best mu, Sigma etc.
%Input:
%y: response variable
%x: input variable: MUST have a column of ones if in form y=b0+b1*x
%alpha: asymmetricness: 0.5=symmetric
%params: intial hyper-parameters

%first beta estimate
N=length(y); 
a=1; b=1; l_one_sigma=0; l_one_sigma2=0; l_one_w=zeros(N,1);
% D=size(X,2); dim=eval(cov()); params=zeros(dim,1); 
%iterate until convergence
EStepIter=50;
MStepIter=50;
% diff_M=zeros(100,1);
D=size(X,2); dim=eval(cov());
% params=initHyp(cov,X,y,dim,params);
for i=1:MStepIter
    K=cov(params(1:(end-1)),X,[]);
    diff=zeros(EStepIter,1);
    for j=1:EStepIter
        [mu, Sigma,gamma,delta]=q_f(y,K,l_one_sigma,l_one_sigma2,l_one_w,alpha);
        [a,b,l_one_sigma,l_one_sigma2]=q_sigma(a,b,gamma,delta,N);
        [alpha_,beta,l_one_w]=q_w(y,mu,Sigma,l_one_sigma2,alpha);
        if j>1
            diff(j)=norm(params_old-[mu; diag(Sigma); a; b; beta],2); 
            if diff(j)<1e-4, break; end
        end
        params_old=[mu; diag(Sigma); a; b; beta];        
    end
%     if mod(i,10)==0, disp(i); end        
%     params=M_step(cov,X,params,mu,Sigma,dim);
    params=M_step(X, y, cov, l_one_w,l_one_sigma,l_one_sigma2,alpha,params);
%     params2=[mu; params];
%     if i>2
%         diff_M(i-1)=max(abs(params_oldM-params2)); 
%         if diff_M(i-1)<1e-5 && i>20
%             break; 
%         end
%     end    
%     params_oldM=params2;
%     disp(i);
end

function [a,b,log_one_sigma,log_one_sigma2]=q_sigma(a,b,gamma,delta,N)
%No analytical solution therefore optimise over log likelihood for a
%inverse gamma distribution
% opts = optimset('MaxIter',1000,'Diagnostics', 'off','GradObj', 'on',...
%                 'Display', 'off','TolFun', 1e-15,'MaxFunEvals', 900);
opts.Display = 0; opts.MaxIter=2000; opts.optTol=1e-10;
par0=log([a;b]);            
[params, ~]=minFunc(@(params)F(params,gamma, delta,N),par0,opts);
lg_a=params(1); lg_b=params(2);                                                     
a=exp(params(1)); b=exp(lg_b); 
%log Expectations
log_one_sigma=lg_a-lg_b;
log_one_sigma2=log_one_sigma+log(a+1)-lg_b;

function [logF, derivative]=F(params,gamma, delta, N)
a=exp(params(1)); b=exp(params(2)); %need and b to be positive 
logF=(a-N)*(log(b)-psi(a))+(b-gamma)*a/b-delta*a*(a+1)/b^2 ...
                                                      -a*log(b)+gammaln(a);
logF=-logF;                                                  
partial_a=(N-a)*psi(1,a)-gamma/b-delta*(2*a+1)/b^2+1;                                                  
ln_partial_b=-N+gamma*a/b+2*delta*a*(a+1)/b^2;   
derivative=-[partial_a*a; ln_partial_b]; 

function [alpha_,beta,l_one_w]=q_w(y,mu,Sigma,l_one_sigma2,alpha)
alpha_=(1-2*alpha)^2/(2*alpha*(1-alpha))+2;
ln_beta=l_one_sigma2+log(alpha)+log((1-alpha)/2)+...
                                          log((y-mu).^2+diag(Sigma));
beta=exp(ln_beta);
l_one_w=0.5*(log(alpha_)-ln_beta);


function [mu, Sigma,gamma,delta]=q_f(y,K,l_one_sigma,l_one_sigma2,l_one_w,alpha)
one_w=exp(l_one_w);
diag_term=(alpha*(1-alpha)/2)*exp(l_one_sigma2)*one_w;
inv_diag_term=1./diag_term;
K_inv_diag=K; step=length(inv_diag_term); 
K_inv_diag(1:(step+1):end)=K_inv_diag(1:(step+1):end)+inv_diag_term';
Sigma=diagMult(K_inv_diag\K,inv_diag_term,'l');
mu=Sigma*(diag_term.*y-0.5*(1-2*alpha)*exp(l_one_sigma));

gamma=-(1-2*alpha)/2*sum((y-mu));
delta=alpha*(1-alpha)/4*sum(one_w.*((y-mu).^2+diag(Sigma)));

% function params=M_step(cov,X,params0,mu,Sigma,dim)
function params=M_step(X, y, cov, l_one_w,l_one_sigma,l_one_sigma2,alpha,params0)
% opts = optimset('MaxIter',200,'Diagnostics', 'off','GradObj', 'off',...
%                 'Display', 'off','TolFun', 1e-10,'MaxFunEvals', 200);
opts.Display = 0; opts.MaxIter=1000; opts.optTol=1e-10;
% params=minFunc(@(cov_par)L_K(cov_par,cov,X,mu,Sigma,dim),params0,opts);
params=minFunc(@(params)L_K2(params, X, y, cov, l_one_w,l_one_sigma,...
                l_one_sigma2,alpha),params0,opts);

% function [lik, negDeriv]=L_K(params,cov,X,mu, Sigma, no_params)
% cov_par=params(1:(end-1)); 
% N=size(X,1);
% ln_sf=cov_par(end); ln_sn=params(end); sigma2=exp(2*ln_sn);
% if ln_sf>2
%     sf2 = exp(2*ln_sf); cov_par(end)=0; sigma2_adj=exp(2*(ln_sn-ln_sf));
%     K=cov(cov_par,X,[]); cov_par(end)=ln_sf;
%     K(1:(N+1):end)=K(1:(N+1):end)+sigma2_adj;
%     [L, e]=jitChol(K); sigma2_adj=sigma2_adj+e; 
%     L=L*exp(ln_sf); sigma2=sigma2_adj*sf2;
% else
%     K=cov(cov_par,X,[]);
%     K(1:(N+1):end)=K(1:(N+1):end)+sigma2;
%     [L, e]=jitChol(K); sigma2=sigma2+e;
% end
% % lambda=eig(K); lambda=real(lambda);
% % if min(lambda)<0 %if slightly negative definite
% %     step=length(K); 
% %     K(1:(step+1):end)=K(1:(step+1):end)-min(lambda)+1e-7;   
% % end
% 
% 
% 
% % d=diag(D);
% % Kinv=diagMult(U,1./d,'r')*U';
% % muTKinvmu=mu'*Kinv*mu;
% Linvmu=L\mu; muTKinvmu=Linvmu'*Linvmu;
% L2=jitChol(Sigma);
% traceTerm=L\L2;
% 
% % len_scales=exp(params(1:(end-2)));
% lik=-sum(log(diag(L)))-0.5*(muTKinvmu+sum(traceTerm(:).^2));
% lik=-real(lik);
% %+sum(exp(len_scales-1e3));
% % disp([lik/1000 cov_par']);
% 
% if nargout>1
%     %derivatives
%     d_len=zeros(no_params+1,1); 
%     for i=1:no_params
%         dK=cov(cov_par,X,X,i); 
%         LinvdK=L\dK;
%         LinvdKLinvT=LinvdK/(L');
%         muTKinvdKKinvmu=Linvmu'*LinvdKLinvT*Linvmu;
% %         KinvdKKinv=Kinv*dK*Kinv;
%         traceDerivative=traceTerm*traceTerm'*LinvdKLinvT;
%         d_len(i)=-0.5*(trace(LinvdKLinvT)-(muTKinvdKKinvmu...
%                                                +trace(traceDerivative)));
%     end
%     %last derivative is wrt to sigma2
% %     KinvdKKinv=Kinv*Kinv;
%     Linv=L\eye(size(K));
%     Kinvmu=L'\Linvmu; KinvL2=L'\traceTerm;
%     d_len(end)=-0.5*(sum(Linv(:).^2)-(Kinvmu'*Kinvmu+sum(KinvL2(:).^2)));
%     d_len(end)=d_len(end)*2*sigma2;
%     negDeriv=-real(d_len);
% end

function params=initHyp(cov,X,y,dim,params0)
opts.Display = 0; opts.MaxIter=100; opts.optTol=1e-10; 
% params0=[params0; rand];
params=minFunc(@(cov_par)initHypLik(cov_par,cov,X,y,dim),params0,opts);
% params=params(1:(end-1)); %dont want the signal noice component

function [lik, negDeriv]=initHypLik(cov_par,cov,X,y,dim)
sn=exp(2*cov_par(end)); step=length(y);
K=cov(cov_par(1:(end-1)),X,[]); K(1:(step+1):end)=K(1:(step+1):end)+sn;

% lambda=eig(K); lambda=real(lambda);
% if min(lambda)<0 %if slightly negative definite
%     step=length(K); 
%     K(1:(step+1):end)=K(1:(step+1):end)-min(lambda)+1e-7;   
% end
% [U, D]=svd(K); d=diag(D);
% Kinv=diagMult(U,1./d,'r')*U';
% yTKinvy=y'*Kinv*y;
L=jitChol(K);
Linvy=L\y;
yTKinvy=Linvy'*Linvy;

lik=-sum(log(diag(L)))-0.5*yTKinvy;
lik=-real(lik);
% disp([lik/1000 cov_par']);

%derivatives
d_len=zeros(dim+1,1); 
for i=1:dim
    dK=cov(cov_par,X,X,i); 
    LinvdK=L\dK;
    LinvdKLinvT=LinvdK/(L');
    yTKinvdKKinvy=Linvy'*LinvdKLinvT*Linvy;
    d_len(i)=-0.5*(trace(LinvdKLinvT)-yTKinvdKKinvy);                                           
end
%wrt sigma2
% dK=eye(length(y));
Linv=L\eye(size(K));
Kinvy=L'\Linvy;
d_len(end)=-0.5*(sum(Linv(:).^2)-(Kinvy'*Kinvy))*2*sn;
% d_len(end+1)=-0.5*(sum(1./d)-norm(Kinv*y)^2)*2*sn;
negDeriv=-real(d_len);

function [lik, negDeriv]=L_K2(params, X, y, cov, l_one_w,l_one_sigma,l_one_sigma2,alpha)
N=size(X,1);
cov_par=params(1:(end-1)); ln_sn=params(end); sigma2=exp(2*ln_sn);
K=cov(cov_par,X,[]); K(1:(N+1):end)=K(1:(N+1):end)+sigma2;

diag_term=(alpha*(1-alpha)/2)*exp(l_one_sigma2+l_one_w);
mu_s=(diag_term.*y-0.5*(1-2*alpha)*exp(l_one_sigma));
inv_diag_term=1./diag_term;
K2=K; K2(1:(N+1):end)=K(1:(N+1):end)+inv_diag_term';
L=jitChol(K2);
e_left=L\(mu_s./diag_term); e_right=L\(K*mu_s);
err=e_left'*e_right;
lik=err/2-sum(log(diag(L)));
lik=-lik;

if nargout>1
    %derivatives
    no_params=length(cov_par); d_len=zeros(no_params+1,1); 
    e_left=L'\e_left;
    e_right=L'\e_right;
    for i=1:no_params
        dK=cov(cov_par,X,X,i); 
%         LinvdK=L\dK;
%         LinvdKLinvT=LinvdK/(L');
%         muTSinvdKSinvmu1=-e_left'*LinvdKLinvT*e_right;
        muTSinvdKSinvmu1=-e_left'*dK*e_right;
%         muTSinvdKSinvmu2=e_left'*LinvdK*mu_s;
        muTSinvdKSinvmu2=e_left'*dK*mu_s;
%         d_len(i)=0.5*((muTSinvdKSinvmu1+muTSinvdKSinvmu2)-trace(LinvdKLinvT));
        d_len(i)=0.5*((muTSinvdKSinvmu1+muTSinvdKSinvmu2)-trace(L'\(L\dK)));
    end
    %last derivative is wrt to sigma2
    LinvdK=L\eye(size(K));    
    LinvdKLinvT=LinvdK/(L');
    muTSinvdKSinvmu1=-e_left'*LinvdKLinvT*e_right;
    muTSinvdKSinvmu2=e_left'*LinvdK*mu_s;
    d_len(end)=0.5*((muTSinvdKSinvmu1+muTSinvdKSinvmu2)-trace(LinvdKLinvT));
    d_len(end)=d_len(end)*2*sigma2;
    negDeriv=-real(d_len);
end