clear all
close all
clc

addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;
addpath(genpath('minFunc_2012'))
warning('off')

alpha=[0.01 0.1 0.5 0.9 0.99];
cov=@covSEard;
iterations=20; rng(1);
VB_MAD=zeros(iterations,length(alpha));
EP_MAD=zeros(iterations,length(alpha));
lin_MAD=zeros(iterations,length(alpha));
lin_VBMAD=zeros(iterations,length(alpha));

VB_RMSE=zeros(iterations,length(alpha));
EP_RMSE=zeros(iterations,length(alpha));
lin_RMSE=zeros(iterations,length(alpha));
lin_VBRMSE=zeros(iterations,length(alpha));

VB_SMSE=zeros(iterations,length(alpha));
EP_SMSE=zeros(iterations,length(alpha));

rng(1);

for i=1: iterations
    x=sort(rand(200,1)*2);
    m=sin(2*pi*x); 
    sigma=sqrt((2.1-x)./4);
    e=gamrnd(0.5,2,length(x),1)-2;
    y=m+sigma.*e;
    for  j=1:length(alpha)
%         figure; plot(x,y,'.'); hold on; %plot(x,m,'r')
        true_q=(2*erfinv(alpha(j))^2-2)*sigma+m; 
%         plot(x,true_q,'g')

        [ell, sf, sn]=initgppar(x,y); 
        [mu,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha(j),[ell; sf; sn]);
        [y_star, var_star]=vbQPredict(cov,x,x,params,mu,Sigma);
%         plot(x,y_star,'m')
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %EM method
        likfunc = @likALD; 
        hyp0.lik = [sn; alpha(j)]; hyp0.cov = [ell; sf];
        hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
        % Prediction
        [ymu, ys2, fmu, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
%         plot(x,fmu,'k')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %linear method
        new_x=[ones(length(x),1) x x.^2 x.^3 x.^4 x.^5 x.^6 x.^7];
        [beta]=rq_fnm(new_x,y,alpha(j));
        q_linear_pred=new_x*beta; %prediction
        %VB linear method
        betaVB=vb3(y,new_x,alpha(j));
        VB_lin=new_x*betaVB;
        % plot(x,q_linear_pred,'r')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

        % legend('observations', 'true quantile', 'VB inference', 'EP inference', 'linear')
        VB_MAD(i,j)=mean(abs(y_star-true_q));
        EP_MAD(i,j)=mean(abs(fmu-true_q));
        lin_MAD(i,j)=mean(abs(q_linear_pred-true_q));
        lin_VBMAD(i,j)=mean(abs(VB_lin-true_q));

        VB_RMSE(i,j)=sqrt(mean((y_star-true_q).^2));
        EP_RMSE(i,j)=sqrt(mean((fmu-true_q).^2));
        lin_RMSE(i,j)=sqrt(mean((q_linear_pred-true_q).^2));
        lin_VBRMSE(i,j)=sqrt(mean((VB_lin-true_q).^2));

        VB_SMSE(i,j)=sqrt(mean((y_star-true_q).^2./var_star));
        EP_SMSE(i,j)=sqrt(mean((fmu-true_q).^2./fs2));

%         fprintf('iteration %d\n',i)
    end
    fprintf('iteration %d\n',i);
end

for j=1:length(alpha)
    fprintf('alpha = %.2f\n',alpha(j));
    disp([mean(VB_MAD(:,j)) std(VB_MAD(:,j)) mean(EP_MAD(:,j)) std(EP_MAD(:,j)) ...
        mean(lin_MAD(:,j)) std(lin_MAD(:,j)) mean(lin_VBMAD(:,j)) std(lin_VBMAD(:,j))]);
    disp([mean(VB_RMSE(:,j)) std(VB_RMSE(:,j)) mean(EP_RMSE(:,j)) std(EP_RMSE(:,j))...
        mean(lin_RMSE(:,j)) std(lin_RMSE(:,j)) mean(lin_VBRMSE(:,j)) std(lin_VBRMSE(:,j))]);    
end
% mean([VB_MAD EP_MAD lin_MAD])
% mean([VB_RMSE EP_RMSE lin_RMSE])
% mean([VB_SMSE EP_SMSE])
% sum(VB_MAD'<EP_MAD')
% sum(VB_MAD'<lin_MAD')
% sum(VB_SMSE<EP_SMSE)

x=sort(rand(200,1)*2);
m=sin(2*pi*x); 
sigma=sqrt((2.1-x)./4);
e=gamrnd(0.5,2,length(x),1)-2;
y=m+sigma.*e;
figure; plot(x,y,'.'); hold on; %plot(x,m,'r')


[ell, sf, sn]=initgppar(x,y); 
for i=1:length(alpha)
    [mu,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha(i),[ell; sf; sn]);
    [y_star, var_star]=vbQPredict(cov,x,x,params,mu,Sigma);
    plot(x,y_star,'m')
    true_q=(2*erfinv(alpha(i))^2-2)*sigma+m; 
    plot(x,true_q,'g--','LineWidth',2);
end
xlabel('x'); ylabel('y'); legend('Observations','Inferred quantile', 'True quantile');

figure; plot(x,y,'.'); hold on; %plot(x,m,'r')
hyp0.cov = [ell; sf];
for i=1:length(alpha)
    hyp0.lik = [sn; alpha(i)];
    hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
    % Prediction
    [ymu, ys2, fmu, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
    plot(x,fmu,'m')
    true_q=(2*erfinv(alpha(i))^2-2)*sigma+m; 
    plot(x,true_q,'g--','LineWidth',2);
end