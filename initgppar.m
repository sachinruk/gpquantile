% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
% consequence of the use of this  material.
%
% Initialise the hyperparameters of a GP using the approach by Snelson
% and Ghahramani
%
% Fabio Ramos 18/08/2010
% [lenscales,sf,noise,affine] = initgppar(X,y)
function [lenscales,sf,noise,affine] = initgppar(X,y)

%mX        = mean(X,2);
%tmp       = bsxfun(@minus,X,mX);
lenscales = initlenscales(X,y);
sf        = 1.5*std(y);
noise     = 0.1*std(y);

if nargout==4
   D = size(X,1);
   affine      = zeros(1,D+1);
   affine(end) = mean(y);
   affine(1:D) = (X*X')\X*(y-affine(end))';
end

% Compute lengthscales based on the average gradient
function [ls] = initlenscales(x,y)
ls=zeros(1,size(x,2));
for i=1:size(x,2)
    X=x(:,i);
    [sX,ind] = sort(X);
    dX       = diff(sX);
    dy       = diff(y(ind));
    tmp      = abs(dX)./abs(dy);
    ls(i)       = mean(tmp);
end