clear all
close all
clc

sigma= 2.3; alpha=0.75;
beta=[2 5];

N=100;
%simulate the errors
u=rand(N,1); w=-log(1-u);
z=randn(N,1);
e=((1-2*alpha)/(alpha*(1-alpha)))*sigma*w+...
                        sqrt(2*w/(alpha*(1-alpha)))*sigma.*z;

%simulate observations
x=[ones(N,1) 10*randn(N,1)+5]; y=x*beta'+e;
tic;
[mu,Sigma,alpha_,beta,a,b]=vb(y,x,alpha);
t=toc;
tic;
[mu2,Sigma2,alpha_2,beta2,a2,b2]=vb2(y,x,alpha);
t2=toc;
disp([t t2])
F(y,mu,Sigma,alpha_,beta,a,b,alpha)

%metropolis
[beta, AR]=metropolis(y,x,alpha, mean(sqrt(diag(Sigma))));
figure; plot(beta(1,:),beta(2,:));
likMetropolis(y,x,beta(:,1000:end),alpha,mu,Sigma)