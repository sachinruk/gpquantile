clear all
close all
clc



addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;
addpath(genpath('minFunc_2012'))
warning('off')

load sp500.mat;
date2=datenum(date,'yyyy-mm-dd');
% figure; plot(date2,close); datetick('x','keepticks','keeplimits')

vol=log(high)-log(low);
% figure; plot(date2,vol); datetick('x','keepticks','keeplimits')

lnret=diff(log(close),1,1);
% figure; plot(date2(2:end),lnret); datetick('x','keepticks','keeplimits');
y=lnret(2:end); x=lnret(1:(end-1));
ytr=y(1:200); xtr=x(1:200);

iterations=1;
alpha=[0.01,0.1 0.5 0.9 0.99]; 
cov=@covLINard;

VBLoss=zeros(iterations,length(alpha));
EPLoss=zeros(iterations,length(alpha));
linLoss=zeros(iterations,length(alpha));


% VBHyp=zeros(3,iterations);
% EPHyp=zeros(3,iterations);
rng(1);
for j=1:length(alpha)
    for i=1:iterations    
        rng(i);
        figure; plot(date2(3:end),y); hold on; datetick('x','keepticks','keeplimits')
        ell=rand; sf=rand; sn=rand;%[ell, sf, sn]=initgppar(x,y); 
        

        %%%%%%%%%%%%%%%%%%%
        %My method
        [mu,Sigma,~,~,~,~,params]=vbGP(ytr,xtr,cov, alpha(j),[ell; sn]);
        [VB_star]=vbQPredict(cov,x,xtr,params,mu,Sigma);
%         VBHyp(:,i)=params;
    %     std_star=sqrt(var_star);
        plot(date2(3:end),VB_star,'m')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %EM method
        likfunc = @likALD;
        hyp0.lik = [sn; alpha(j)]; hyp0.cov = [ell];
        hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, ...
                                                           xtr, ytr,0);
        % Prediction
        [~, ~, EP_star] = gp(hyp, @infEP, [], cov, likfunc, xtr, ytr, x);
%         EPHyp(:,i)=[hyp.cov; hyp.lik(1)];
        plot(date2(3:end),EP_star,'k')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %linear method
        new_x=[ones(length(xtr),1) xtr xtr.^2 xtr.^3 xtr.^4 xtr.^5 xtr.^6 xtr.^7];
        [beta]=rq_fnm(new_x,ytr,alpha(j));
        x_query=[ones(length(x),1) x x.^2 x.^3 x.^4 x.^5 x.^6 x.^7];
        q_linear_pred=x_query*beta; %prediction
        plot(date2(3:end),q_linear_pred,'r')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

%         legend('observations', 'VB inference', 'EP inference', 'linear')

        VBLoss(i,j)=pinballLoss(y,VB_star,alpha(j));
        EPLoss(i,j)=pinballLoss(y,EP_star,alpha(j));   
        linLoss(i,j)=pinballLoss(y,q_linear_pred,alpha(j));
%         disp(i);
    end
    fprintf('alpha = %.2f\n',alpha(j));
    disp([mean(VBLoss(:,j)) std(VBLoss(:,j)) mean(EPLoss(:,j)) std(EPLoss(:,j)) mean(linLoss(:,j)) std(linLoss(:,j))]);
end

figure; plot(x,y,'.'); hold on;
ell=rand; sf=rand; sn=rand; 
for i=1:length(alpha)
    [mu,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha(i),[ell; sf; sn]);
    [VB_star]=vbQPredict(cov,x,x,params,mu,Sigma);    
    plot(x,VB_star,'r-.','LineWidth',2);
end
xlabel('x'); ylabel('y'); legend('Observations','Inferred quantile');

figure; plot(x,y,'.'); hold on; %plot(x,m,'r')
hyp0.cov = rand(2,1);
for i=1:length(alpha)
    hyp0.lik = [rand; alpha(i)];
    hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
    [ymu, ys2, fmu, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
    plot(x,fmu,'r-.','LineWidth',2)
end
xlabel('x'); ylabel('y'); legend('Observations','Inferred quantile');
