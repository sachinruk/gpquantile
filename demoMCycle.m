clear all
close all
clc

addpath('../GP/gpml-matlab-v3.2-2013-01-15/'); startup;
addpath(genpath('minFunc_2012'))
warning('off')

% Silverman's motorcycle benchmark dataset
data = [  2.4    0.0 ;  2.6   -1.3 ;  3.2   -2.7 ;  3.6    0.0 ;
          4.0   -2.7 ;  6.2   -2.7 ;  6.6   -2.7 ;  6.8   -1.3 ;
          7.8   -2.7 ;  8.2   -2.7 ;  8.8   -1.3 ;  8.8   -2.7 ;
          9.6   -2.7 ; 10.0   -2.7 ; 10.2   -5.4 ; 10.6   -2.7 ;
         11.0   -5.4 ; 11.4    0.0 ; 13.2   -2.7 ; 13.6   -2.7 ;
         13.8    0.0 ; 14.6  -13.3 ; 14.6   -5.4 ; 14.6   -5.4 ;
         14.6   -9.3 ; 14.6  -16.0 ; 14.6  -22.8 ; 14.8   -2.7 ;
         15.4  -22.8 ; 15.4  -32.1 ; 15.4  -53.5 ; 15.4  -54.9 ;
         15.6  -40.2 ; 15.6  -21.5 ; 15.8  -21.5 ; 15.8  -50.8 ;
         16.0  -42.9 ; 16.0  -26.8 ; 16.2  -21.5 ; 16.2  -50.8 ;
         16.2  -61.7 ; 16.4   -5.4 ; 16.4  -80.4 ; 16.6  -59.0 ;
         16.8  -71.0 ; 16.8  -91.1 ; 16.8  -77.7 ; 17.6  -37.5 ;
         17.6  -85.6 ; 17.6 -123.1 ; 17.6 -101.9 ; 17.8  -99.1 ;
         17.8 -104.4 ; 18.6 -112.5 ; 18.6  -50.8 ; 19.2 -123.1 ;
         19.4  -85.6 ; 19.4  -72.3 ; 19.6 -127.2 ; 20.2 -123.1 ;
         20.4 -117.9 ; 21.2 -134.0 ; 21.4 -101.9 ; 21.8 -108.4 ;
         22.0 -123.1 ; 23.2 -123.1 ; 23.4 -128.5 ; 24.0 -112.5 ;
         24.2  -95.1 ; 24.2  -81.8 ; 24.6  -53.5 ; 25.0  -64.4 ;
         25.0  -57.6 ; 25.4  -72.3 ; 25.4  -44.3 ; 25.6  -26.8 ;
         26.0   -5.4 ; 26.2 -107.1 ; 26.2  -21.5 ; 26.4  -65.6 ;
         27.0  -16.0 ; 27.2  -45.6 ; 27.2  -24.2 ; 27.2    9.5 ;
         27.6    4.0 ; 28.2   12.0 ; 28.4  -21.5 ; 28.4   37.5 ;
         28.6   46.9 ; 29.4  -17.4 ; 30.2   36.2 ; 31.0   75.0 ;
         31.2    8.1 ; 32.0   54.9 ; 32.0   48.2 ; 32.8   46.9 ;
         33.4   16.0 ; 33.8   45.6 ; 34.4    1.3 ; 34.8   75.0 ;
         35.2  -16.0 ; 35.2  -54.9 ; 35.4   69.6 ; 35.6   34.8 ;
         35.6   32.1 ; 36.2  -37.5 ; 36.2   22.8 ; 38.0   46.9 ;
         38.0   10.7 ; 39.2    5.4 ; 39.4   -1.3 ; 40.0  -21.5 ;
         40.4  -13.3 ; 41.6   30.8 ; 41.6  -10.7 ; 42.4   29.4 ;
         42.8    0.0 ; 42.8  -10.7 ; 43.0   14.7 ; 44.0   -1.3 ;
         44.4    0.0 ; 45.0   10.7 ; 46.6   10.7 ; 47.8  -26.8 ;
         47.8  -14.7 ; 48.8  -13.3 ; 50.6    0.0 ; 52.0   10.7 ;
         53.2  -14.7 ; 55.0   -2.7 ; 55.0   10.7 ; 55.4   -2.7 ;
         57.6   10.7 ];

x=data(:,1); y=data(:,2); y=(y-mean(y))/std(y);

iterations=20;
alpha=[0.01,0.1 0.5 0.9 0.99]; 
cov=@covSEard;
likfunc = @likALD;

VBLoss=zeros(iterations,length(alpha));
EPLoss=zeros(iterations,length(alpha));
linLoss=zeros(iterations,length(alpha));

VBPLoss=zeros(iterations,length(alpha));
EPPLoss=zeros(iterations,length(alpha));
linPLoss=zeros(iterations,length(alpha));
% 
% est_alphaVB=zeros(iterations,length(alpha));
% est_alphaEP=zeros(iterations,length(alpha));
% est_alphaLin=zeros(iterations,length(alpha));

% VBHyp=zeros(3,iterations);
% EPHyp=zeros(3,iterations);
rng(1);
for i=1:iterations
    ell=rand; sf=rand; sn=rand;%[ell, sf, sn]=initgppar(x,y); 
    idx=randperm(length(x)); train_idx=idx(1:106); test_idx=idx(107:end);
    xtr=x(train_idx,:); ytr=y(train_idx);
    x_test=x(test_idx,:); y_test=y(test_idx);
%     figure; plot(xtr,ytr,'.'); xlabel('Age'); ylabel('Density'); hold on;  
    for j=1:length(alpha)    
        %%%%%%%%%%%%%%%%%%%
        %My method
        [mu,Sigma,~,~,~,~,params]=vbGP(ytr,xtr,cov, alpha(j),[ell; sf; sn]);
        [VB_star]=vbQPredict(cov,x_test,xtr,params,mu,Sigma);
%         plot(x,VB_star,'m')

        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %EM method
        hyp0.lik = [sn; alpha(j)]; hyp0.cov = [ell; sf];
        hyp=minimize2([hyp0.cov; hyp0.lik], 4000, @infEP, [], cov, likfunc, ...
                                                           xtr, ytr,0);
        % Prediction
        try
            [~, ~, EP_star] = gp(hyp, @infEP, [], cov, likfunc, xtr, ytr, x_test);
%             plot(x,EP_star,'k');
        catch
            EP_star=nan(size(VB_star));
        end
%         EPHyp(:,i)=[hyp.cov; hyp.lik(1)];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %linear method
        new_x=[ones(length(xtr),1) xtr xtr.^2 xtr.^3 xtr.^4 xtr.^5 xtr.^6 xtr.^7];
        [beta]=rq_fnm(new_x,ytr,alpha(j));
        x_query=ones(length(x_test),1);
        for k=1:7, x_query=[x_query x_test.^k]; end
        q_linear_pred=x_query*beta; %prediction
%         plot(x,q_linear_pred,'r')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

        VBLoss(i,j)=pinballLoss(y_test,VB_star,alpha(j));
        EPLoss(i,j)=pinballLoss(y_test,EP_star,alpha(j));   
        linLoss(i,j)=pinballLoss(y_test,q_linear_pred,alpha(j));
        
        VBPLoss(i,j)=percentageLoss(y_test,VB_star,alpha(j));
        EPPLoss(i,j)=percentageLoss(y_test,EP_star,alpha(j));   
        linPLoss(i,j)=percentageLoss(y_test,q_linear_pred,alpha(j));
        
%         est_alphaVB(i,j)=sum(y_test<VB_star)/length(y_test);
%         est_alphaEP(i,j)=sum(y_test<EP_star)/length(y_test);
%         est_alphaLin(i,j)=sum(y_test<q_linear_pred)/length(y_test);
    end
%     legend('Observations','VB Quantiles','EP Quantiles');
    disp(i);
end

for j=1:length(alpha)
    disp([mean(VBLoss(:,j)) std(VBLoss(:,j)) mean(EPLoss(:,j)) std(EPLoss(:,j)) mean(linLoss(:,j)) std(linLoss(:,j))]);
    disp([mean(VBPLoss(:,j)) std(VBPLoss(:,j)) mean(EPPLoss(:,j)) std(EPPLoss(:,j)) mean(linPLoss(:,j)) std(linPLoss(:,j))]);
end

% figure; plot(x,y,'.'); hold on;
% ell=rand; sf=rand; sn=rand; 
% params=[ell; sf; sn];
% for i=1:length(alpha)
%     [mu,Sigma,~,~,~,~,params]=vbGP(y,x,cov, alpha(i),params);
%     [VB_star]=vbQPredict(cov,x,x,params,mu,Sigma);    
%     plot(x,VB_star,'r','LineWidth',2);
% end
% xlabel('t (s)'); ylabel('Acc (m/s^2)'); legend('Observations','Inferred quantile');
% 
% figure; plot(x,y,'.'); hold on; %plot(x,m,'r')
% hyp.cov = rand(2,1);
% hyp.lik = [rand; alpha(1)];
% for i=1:length(alpha)
%     hyp.lik(2)= alpha(i);
%     hyp=minimize2([hyp.cov; hyp.lik], 4000, @infEP, [], cov, likfunc, x, y,0);
%     [ymu, ys2, fmu, fs2] = gp(hyp, @infEP, [], cov, likfunc, x, y, x);
%     plot(x,fmu,'r','LineWidth',2)
% end
% xlabel('t (s)'); ylabel('Acc (m/s^2)'); legend('Observations','Inferred quantile');