function hyp=minimize2(hyp0, iter, inf, mean_, cov, lik, x, y,penalise)

% opts = optimset('MaxIter',iter,...
%                 'Diagnostics', 'on',...
%                 'GradObj', 'on',...
%                 'Display', 'on',...
%                 'TolFun',  1e-15,...
%                 'MaxFunEvals', 900);
opts = []; opts.display = 'none'; opts.maxFunEvals =iter;
sn_boundary=0.25*log(mean(y.^2));
hyp_=minFunc(@(hyp)gp_wrapper(hyp, inf, mean_, cov, lik, x, y,sn_boundary,penalise),hyp0,opts);
D=size(x,2);
hyp.cov=hyp_(1:eval(cov())); hyp.lik=hyp_((eval(cov())+1):end);
% if strcmp(cov(),'(D+1)')
%     hyp.cov=hyp_(1:(len+1)); %length scales + signal variance parameter
%     hyp.lik=hyp_((len+2):end);
% else
%     hyp.cov=hyp_(1:len); %length scales + signal variance parameter
%     hyp.lik=hyp_((len+1):end);
% end

function [nlik, dnlik]=gp_wrapper(hyp_s,inf, mean, cov, lik, x, y,sn_boundary,penalise)
D=size(x,2);
hyp.cov=hyp_s(1:eval(cov())); hyp.lik=hyp_s((eval(cov())+1):end);
% if strcmp(cov(),'(D+1)')
%     hyp.cov=X(1:(D+1)); %length scales + signal variance parameter
%     hyp.lik=X((D+2):end);
% else
%     hyp.cov=X(1:D); %length scales + signal variance parameter
%     hyp.lik=X((D+1):end);
% end

% myfunc(hyp,inf, mean, cov, lik, x, y);
% disp(hyp); disp(hyp.cov);
k=100*penalise;
[nlik, dlik]=gp(hyp,inf, mean, cov, lik, x, y);
nlik=nlik+k*exp(hyp.lik(end)-sn_boundary);
dlik.lik(end)=dlik.lik(end)+k*exp(hyp.lik(end)-sn_boundary);
dnlik(1:eval(cov()))=dlik.cov; 
dnlik((eval(cov())+1):(eval(cov())+length(dlik.lik)))=dlik.lik;
% if strcmp(cov(),'(D+1)')
%     dnlik(1:(D+1))=dlik.cov;
%     dnlik((D+2):length(X))=dlik.lik;
% else
%     dnlik(1:D)=dlik.cov;
%     dnlik((D+1):length(X))=dlik.lik;
% end
    
dnlik=dnlik';


% function myfunc(hyp,inf, mean, cov, lik, x, y)
% disp(hyp);